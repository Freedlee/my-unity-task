﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject enemy;
    public Vector3 spawnValues;
    public float spawnWait;
    public int startWait;
    public float mostWait;
    public float leastWait;

    void Start()
    {
        StartCoroutine(CreateObj());
    }
    private void FixedUpdate()
    {
        spawnWait = Random.Range(leastWait, mostWait);
    }

    IEnumerator CreateObj()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), 1, Random.Range(-spawnValues.z, spawnValues.z));
            Instantiate(enemy, spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            yield return new WaitForSeconds(spawnWait);
        }

    }
}