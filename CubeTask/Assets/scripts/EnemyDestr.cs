﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestr : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet" )
        {
            Destroy(gameObject);
        }
    }
}
